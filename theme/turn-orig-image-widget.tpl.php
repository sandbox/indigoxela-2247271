<?php
/* Available variables: $element array */
drupal_add_library('system', 'drupal.collapse');
?>
<?php if (isset($element['preview'])): ?>
<div class="imagefield-turn-preview">
  <?php print $element['preview']['#markup']; ?>
</div>
<?php endif; ?>

<?php if (isset($element['turnbox'])): ?>
<fieldset class="collapsible collapsed">
  <legend><span class="fieldset-legend"><?php print $element['legend']; ?></span></legend>
  <div class="turnbox-wrapper fieldset-wrapper">
    <div class="imagefield-turn-turnbox">
      <?php print $element['turnbox']['#markup']; ?>
    </div>
  <?php print $element['hint']; ?>
  </div>
</fieldset>
<?php endif; ?>
<?php print $element['form_elems']; ?>
