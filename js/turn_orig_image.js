(function ($) {
/**
 * @file
 * turn_orig_image module js
 *
 * JS for turning image widget
 */
Drupal.behaviors.turn_orig_image = {

  attach: function (context, settings) {

    if ($('.turnbox', context).length === 0) {
      /* no turnbox, probably an image upload (http://drupal.org/node/366296) */
      return;
    }

    /* add turn exactly once to each turnbox */
    $('.turnbox', context).once(function() {
      var self = $(this);

      /* get the id attribute for multiple image support */
      var self_id = self.attr('id');
      var id = self_id.substring(0, self_id.indexOf('-turnbox'));
      /* get the name attribute for imagefield name */
      var widget = self.closest('.form-type-managed-file');

      var angle = 0;
      $(this).click(function () {
        $(this).removeClass();
        angle = parseInt(angle + 90, 10);
        if (angle >= 360) {
          angle = 0;
        }
        var cssClass = 'turnbox-' + angle;
        widget.find("legend span.summary").text(' - ' + angle + '°');
        $(this).addClass(cssClass);
        if (angle > 0) {
          $(".edit-image-turn-angle", widget).val(angle);
          $(".edit-image-turn-changed", widget).val(1);
        } else {
          $(".edit-image-turn-angle", widget).val(0);
          $(".edit-image-turn-changed", widget).val(0);
        }

      });
    });
  }
};

})(jQuery);